import java.util.Arrays;
import java.util.Random;

public class Main {

    static class Heap {

        final static int MAX_HEAP_VALUES = 1000;

        HeapElement[] values = new HeapElement[MAX_HEAP_VALUES];
        private static final Random rand = new Random();  /* why this is not working? */
        HeapElement tempValue;
        int size;

        class HeapElement {
            int value;

            HeapElement(int value) {
                this.value = value;
            }

        }

        // TODO: 06.10.2018 toString method 

        public HeapElement extractMax() {



            int leftChildIndex = 1;
            int rightChildIndex = 2;
            int parentIndex = 0;
            HeapElement result;

            result = values[0];
            values[0] = values[size - 1];
            values[size - 1] = null;
            size--;

            while (values[parentIndex].value < values[leftChildIndex].value || values[parentIndex].value < values[rightChildIndex].value) {

//              Moving parent down the tree.
                if (values[leftChildIndex].value > values[rightChildIndex].value) {
                    tempValue = values[leftChildIndex];
                    values[leftChildIndex] = values[parentIndex];
                    values[parentIndex] = tempValue;
                    parentIndex = leftChildIndex;
                    leftChildIndex = parentIndex * 2;
                    rightChildIndex = leftChildIndex + 1;
                } else {
                    tempValue = values[rightChildIndex];
                    values[rightChildIndex] = values[parentIndex];
                    values[parentIndex] = tempValue;
                    parentIndex = rightChildIndex;
                    leftChildIndex = parentIndex * 2;
                    rightChildIndex = leftChildIndex + 1;
                }

                // TODO: 06.10.2018 check how to avoid ArrayOutofoBonds
                if (values[rightChildIndex] == null || values[leftChildIndex] == null) {
                    return result;
                }
            }
            return result;
        }

        public HeapElement heapSort() {

            for (int i = 0; i < size; i++) {
                extractMax();

            }
            return values[0];

        }

        public void add(int inputNumbers) {

            int parentValue;
            int actualValue;
            int tempParentValue;

            if (size == 0) {
                createNewHeaplement(inputNumbers);
                return;
            }

            createNewHeaplement(inputNumbers);
            parentValue = (size - 1) / 2;
            actualValue = size - 1;

            // sorting and swapping elements - of necessary.
            while (values[actualValue].value > values[parentValue].value) {
                tempValue = values[parentValue];
                values[parentValue] = values[actualValue];
                values[actualValue] = tempValue;
                tempParentValue = parentValue;
                parentValue = (parentValue - 1) / 2;
                actualValue = tempParentValue;

            }
        }

        private void createNewHeaplement(int inputNumbers) {
            try {
                HeapElement obj = new HeapElement(inputNumbers);
                values[size] = obj;
                size++;
            } catch (ArrayIndexOutOfBoundsException a) {
                System.out.println("Przekroczono maksymalną liczbę elementów");
            }
        }
    }

    public static void main(String[] args) {

        Heap heap = new Heap();


        for (int i = 0; i < 10; i++) {
//                heap.add();
            heap.add(i);
        }

        System.out.println(heap);
        System.out.println("******");
        System.out.println(heap.extractMax());
        System.out.println("******");
        System.out.println(heap);
        System.out.println("******");
        System.out.println(heap.heapSort());

    }
}
// Dziekujemy za uwagę.